# CVA6 - Simulation flow and RVFI Tracing

## 1. Getting started quickly

```
git clone https://github.com/openhwgroup/cva6.git
git submodule update --init --recursive
```

### Verilator Simulation flow

1. `export RISCV=/YOUR/TOOLCHAIN/INSTALLATION/DIRECTORY`

2. Modify the `TOP` [line 4] variable as per requirement, and run `source ci/path-setup.sh`

3. Run `./ci/gitlab-ci-emul.sh`

4. Build the model of CVA6 by `make verilate`

OR
To enable support for vcd files, run `make verilate DEBUG=1`


>Verilator version given in the upstream repo shows some issues. Modified file for workable installation can be found in this repo. Modifications are discussed at the end of this file.



## 2. Running a program

Binary will be generated in work-ver directory and accepts a RISC-V ELF binary as an argument

```
work-ver/Variane_testharness <elf-file-name>
```

This will generate a .dasm file, which we could feed the log to spike-dasm to resolve instructiones to mnemonics.

>Unfortunately value inspection is currently not possible for the Verilator trace file.

```
spike-dasm < trace_hart_00.dasm > logfile.txt
```

To run user-space applications, check https://github.com/openhwgroup/cva6#running-user-space-applications


## 3. RISC-V Formal Interface (RVFI) Tracing

The logs generated with the simulation comes from the rvfi-tracer.



RVFI Tracer can be found at `corev_apu/tb/rvfi_tracer.sv`

Structure of RVFI Tracer

`rvfi_tracer` module:

| Parameters | Inputs |
| ------ | ------ |
|HART_ID|clk_i|
|DEBUG_START|rst_ni|
|NR_COMMIT_PORTS|rvfi_i|
|DEBUG_STOP|        |


The working of `rvfi_tracer`:

1. Posedge of clock will trigger a loop.
2. PC64 is calculated.
3. Instruction information is being printed.

A sequential logic which gets triggered on the positive edge of clk_i is implemented at line 34.

pc64 is calculated according to the configuration we give on line 36.

Instruction information is being written to the log file at line 40.

The package will handle both uncompressed and compressed instructions [line 52]. 




The `rvfi_instr_t` package can be found at `corev_apu/tb/rvfi_pkg.sv`

>NRET = Number of RVFI Channels  
ILEN = Instruction length   
XLEN = Processor data length


3.1 The specification of`rvfi_instr_t`


```
  localparam NRET = 1;
  localparam ILEN = 32;

  typedef struct packed {
    logic [NRET-1:0]                 valid;
    logic [NRET*64-1:0]              order;
    logic [NRET*ILEN-1:0]            insn;
    logic [NRET-1:0]                 trap;
    logic [NRET*riscv::XLEN-1:0]     cause;
    logic [NRET-1:0]                 halt;
    logic [NRET-1:0]                 intr;
    logic [NRET*2-1:0]               mode;
    logic [NRET*2-1:0]               ixl;
    logic [NRET*5-1:0]               rs1_addr;
    logic [NRET*5-1:0]               rs2_addr;
    logic [NRET*riscv::XLEN-1:0]     rs1_rdata;
    logic [NRET*riscv::XLEN-1:0]     rs2_rdata;
    logic [NRET*5-1:0]               rd_addr;
    logic [NRET*riscv::XLEN-1:0]     rd_wdata;

    logic [NRET*riscv::XLEN-1:0]     pc_rdata;
    logic [NRET*riscv::XLEN-1:0]     pc_wdata;

    logic [NRET*riscv::XLEN-1:0]     mem_addr;
    logic [NRET*(riscv::XLEN/8)-1:0] mem_rmask;
    logic [NRET*(riscv::XLEN/8)-1:0] mem_wmask;
    logic [NRET*riscv::XLEN-1:0]     mem_rdata;
    logic [NRET*riscv::XLEN-1:0]     mem_wdata;
```

The following details can be found at https://github.com/SymbioticEDA/riscv-formal/blob/master/docs/rvfi.md

### 3.1.1 RVFI Specification

In the following specification the term XLEN refers to the width of an x register in bits, as described in the RISC-V ISA specification. The term NRET refers to the maximum number of instructions that the core under test can retire in one cycle. If more than one of the retired instruction writes the same register, the channel with the highest index contains the instruction that wins the conflict. The term ILEN refers to the maximum instruction width for the processor under test.

The Interface consists only of output signals. Each signal is a concatenation of NRET values of constant width, effectively creating NRET channels. For simplicity, the following descriptions refer to one such channel. For example, we refer to rvfi_valid as a 1-bit signal, not a NRET-bits signal.
Instruction Metadata

output [NRET        - 1 : 0] rvfi_valid
output [NRET *   64 - 1 : 0] rvfi_order
output [NRET * ILEN - 1 : 0] rvfi_insn
output [NRET        - 1 : 0] rvfi_trap
output [NRET        - 1 : 0] rvfi_halt
output [NRET        - 1 : 0] rvfi_intr
output [NRET * 2    - 1 : 0] rvfi_mode
output [NRET * 2    - 1 : 0] rvfi_ixl

When the core retires an instruction, it asserts the rvfi_valid signal and uses the signals described below to output the details of the retired instruction. The signals below are only valid during such a cycle and can be driven to arbitrary values in a cycle in which rvfi_valid is not asserted.

The rvfi_order field must be set to the instruction index. No indices must be used twice and there must be no gaps. Instructions may be retired in a reordered fashion, as long as causality is preserved (register and memory write operations must be retired before the read operations that depend on them).

rvfi_insn is the instruction word for the retired instruction. In case of an instruction with fewer than ILEN bits, the upper bits of this output must be all zero. For compressed instructions the compressed instruction word must be output on this port. For fused instructions the complete fused instruction sequence must be output.

rvfi_trap must be set for an instruction that cannot be decoded as a legal instruction, such as 0x00000000.

In addition, rvfi_trap must be set for a misaligned memory read or write in PMAs that don't allow misaligned access, or other memory access violations. rvfi_trap must also be set for a jump instruction that jumps to a misaligned instruction.

The signal rvfi_halt must be set when the instruction is the last instruction that the core retires before halting execution. It should not be set for an instruction that triggers a trap condition if the CPU reacts to the trap by executing a trap handler. This signal enables verification of liveness properties.

rvfi_intr must be set for the first instruction that is part of a trap handler, i.e. an instruction that has a rvfi_pc_rdata that does not match the rvfi_pc_wdata of the previous instruction.

rvfi_mode must be set to the current privilege level, using the following encoding: 0=U-Mode, 1=S-Mode, 2=Reserved, 3=M-Mode

Finally rvfi_ixl must be set to the value of MXL/SXL/UXL in the current privilege level, using the following encoding: 1=32, 2=64
Integer Register Read/Write

output [NRET *    5 - 1 : 0] rvfi_rs1_addr
output [NRET *    5 - 1 : 0] rvfi_rs2_addr
output [NRET * XLEN - 1 : 0] rvfi_rs1_rdata
output [NRET * XLEN - 1 : 0] rvfi_rs2_rdata

rvfi_rs1_addr and rvfi_rs2_addr are the decoded rs1 and rs1 register addresses for the retired instruction. For an instruction that reads no rs1/rs2 register, this output can have an arbitrary value. However, if this output is nonzero then rvfi_rs1_rdata must carry the value stored in that register in the pre-state.

rvfi_rs1_rdata/rvfi_rs2_rdata is the value of the x register addressed by rs1/rs2 before execution of this instruction. This output must be zero when rs1/rs2 is zero.

output [NRET *    5 - 1 : 0] rvfi_rd_addr
output [NRET * XLEN - 1 : 0] rvfi_rd_wdata

rvfi_rd_addr is the decoded rd register address for the retired instruction. For an instruction that writes no rd register, this output must always be zero.

rvfi_rd_wdata is the value of the x register addressed by rd after execution of this instruction. This output must be zero when rd is zero.
Program Counter

output [NRET * XLEN - 1 : 0] rvfi_pc_rdata
output [NRET * XLEN - 1 : 0] rvfi_pc_wdata

This is the program counter (pc) before (rvfi_pc_rdata) and after (rvfi_pc_wdata) execution of this instruction. I.e. this is the address of the retired instruction and the address of the next instruction.
Memory Access

output [NRET * XLEN   - 1 : 0] rvfi_mem_addr
output [NRET * XLEN/8 - 1 : 0] rvfi_mem_rmask
output [NRET * XLEN/8 - 1 : 0] rvfi_mem_wmask
output [NRET * XLEN   - 1 : 0] rvfi_mem_rdata
output [NRET * XLEN   - 1 : 0] rvfi_mem_wdata

For memory operations (rvfi_mem_rmask and/or rvfi_mem_wmask are non-zero), rvfi_mem_addr holds the accessed memory location.

When the define RISCV_FORMAL_ALIGNED_MEM is set, the address must have a 4-byte alignment for XLEN=32 and an 8-byte alignment for XLEN=64. When the define is not set, then the address must point directly to the LSB or the word / half word / byte that is accessed.

rvfi_mem_rmask is a bitmask that specifies which bytes in rvfi_mem_rdata contain valid read data from rvfi_mem_addr.

rvfi_mem_wmask is a bitmask that specifies which bytes in rvfi_mem_wdata contain valid data that is written to rvfi_mem_addr.

rvfi_mem_rdata is the pre-state data read from rvfi_mem_addr. rvfi_mem_rmask specifies which bytes are valid.

rvfi_mem_wdata is the post-state data written to rvfi_mem_addr. rvfi_mem_wmask specifies which bytes are valid.

When RISCV_FORMAL_ALIGNED_MEM is set then riscv-formal assumes that unaligned memory access causes a trap.






`core/include/riscv_pkg.sv` contains the config parameters from cva6_config_pkg

The `rvfi_i` required as input comes from the above struct found at `rvfi_pkg.sv`

We could trace any custom signals by adding them into debug and name arrays. [line 105]

The following exceptions can be found in the logs [line 80]:

>- INSTR_ADDR_MISALIGNED: Instruction Address Misaligned
>- INSTR_ACCESS_FAULT: Instruction Access Fault
>- ILLEGAL_INSTR: Illegal Instruction
>- BREAKPOINT: Breakpoint
>- LD_ADDR_MISALIGNED: Load Address Misaligned
>- LD_ACCESS_FAULT: Load Access Fault
>- ST_ADDR_MISALIGNED: Store Address Misaligned
>- ST_ACCESS_FAULT: Store Access Fault

`cva6/common/local/util/ex_trace_item.svh` contains much more robust single exception items



`ariane_testharness.sv` line 662
  ```
  rvfi_tracer  #(
    .HART_ID(hart_id),
    .DEBUG_START(0),
    .DEBUG_STOP(0)
  ) rvfi_tracer_i (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .rvfi_i(rvfi)
  );
  ```

The traces are being captured from this module call.


## 4 RVFI Output 


The output signal for the module call aforementioned is returned at the `core/cva6.sv` [line 56] if RVFI_TRACE == 1.

Issue and Execution stages gives the following Memory Access Inputs and Outputs respectively for RVFI. [cva6.sv]

| Inputs | Outputs |
| ------ | ------ |
|lsu_addr_i|lsu_addr_o|
|lsu_rmask_i|lsu_rmask_o|
|lsu_wmask_i|lsu_wmask_o|
|lsu_addr_trans_id_i|lsu_addr_trans_id_o|

These Load Store Unit registers are given to the RVFI as Memory Access (RVF_MEM)
See [line 1013]. A snippet is given below to recognize the signals. The specification can be found at 3.1.1

### 4.1 Signals recieved by RVFI

The following code can be found at `core/cva6.sv`
```
rvfi_o[i].valid    = (commit_ack[i] && !ex_commit.valid) ||
        (exception && (ex_commit.cause == riscv::ENV_CALL_MMODE ||
                  ex_commit.cause == riscv::ENV_CALL_SMODE ||
                  ex_commit.cause == riscv::ENV_CALL_UMODE));
      rvfi_o[i].insn     = ex_commit.valid ? ex_commit.tval[31:0] : commit_instr_id_commit[i].ex.tval[31:0];
      // when trap, the instruction is not executed
      rvfi_o[i].trap     = mem_exception;
      rvfi_o[i].cause    = ex_commit.cause;
      rvfi_o[i].mode     = debug_mode ? 2'b10 : priv_lvl;
      rvfi_o[i].ixl      = riscv::XLEN == 64 ? 2 : 1;
      rvfi_o[i].rs1_addr = commit_instr_id_commit[i].rs1;
      rvfi_o[i].rs2_addr = commit_instr_id_commit[i].rs2;
      rvfi_o[i].rd_addr  = commit_instr_id_commit[i].rd;
      rvfi_o[i].rd_wdata = ariane_pkg::is_rd_fpr(commit_instr_id_commit[i].op) == 0 ? wdata_commit_id[i] : commit_instr_id_commit[i].result;
      rvfi_o[i].pc_rdata = commit_instr_id_commit[i].pc;
`ifdef RVFI_MEM
      rvfi_o[i].mem_addr  = commit_instr_id_commit[i].lsu_addr;
      rvfi_o[i].mem_wmask = commit_instr_id_commit[i].lsu_wmask;
      rvfi_o[i].mem_wdata = commit_instr_id_commit[i].lsu_wdata;
      rvfi_o[i].mem_rmask = commit_instr_id_commit[i].lsu_rmask;
      rvfi_o[i].mem_rdata = commit_instr_id_commit[i].result;
      rvfi_o[i].rs1_rdata = commit_instr_id_commit[i].rs1_rdata;
      rvfi_o[i].rs2_rdata = commit_instr_id_commit[i].rs2_rdata;
```

### Stage-wise signals to RVFI Output


### 4.2 Execution Stage

It outputs the signals `lsu_addr_o`, `lsu_rmask_o`, `lsu_wmask_o`, `lsu_addr_trans_id_o`.

The signals comes from the `core/load_store_unit.sv` [line 469].

```
    assign lsu_addr_o = lsu_ctrl.vaddr;
    assign lsu_rmask_o = lsu_ctrl.fu == LOAD ? lsu_ctrl.be : '0;
    assign lsu_wmask_o = lsu_ctrl.fu == STORE ? lsu_ctrl.be : '0;
    assign lsu_addr_trans_id_o = lsu_ctrl.trans_id;
```
### 4.3 Issue Stage

The issue stage takes the aforementioned signals as inputs. 

`core/issue_stage.sv` [line 83]

### 4.4 Commit Stage

In the line `rvfi_o[i].mem_addr  = commit_instr_id_commit[i].lsu_addr;`, the definition of `commit_instr_id_commit` comes from the Commit stage, which is used to write the information to RVFI.

See [line 175] (ID <-> COMMIT Signals) where the instantiation of module happens.

`scoreboard_entry_t [NR_COMMIT_PORTS-1:0] commit_instr_id_commit;`

`core/include/ariane_pkg.sv` defines the package `scoreboard_entry_t`

For the RVFI part:
        ```
        riscv::xlen_t               rs1_rdata;
        riscv::xlen_t               rs2_rdata;
        logic [riscv::VLEN-1:0]     lsu_addr;
        logic [(riscv::XLEN/8)-1:0] lsu_rmask;
        logic [(riscv::XLEN/8)-1:0] lsu_wmask;
        riscv::xlen_t               lsu_wdata;
        ```


### Scoreboard

It happens in the issue stage. It takes the following signals as input from Issue stage and, RVFI utilizes the same.

`lsu_addr_i`, `lsu_rmask_i`, `lsu_wmask_i`, `lsu_addr_trans_id_i`, `rs1_forwarding_i`, `rs2_forwarding_i`.

These signals are being input to the ex_stage at `core/cva6.sv` [line 420].



>The current scope of this documentation is restricted to RVFI and tracing using the same.

>There are various methods like FireSim trace, Dromajo and Verilator tracing within the CVA6 Codebase. 



Troubleshooting

1. sudo apt-get install device-tree-compiler - during fesvr installation

2. Clone into submodule path 'riscv-qemu' failed; github.com errno=Connection refused.

3. installation of verilator 4.110, other versions may not work.
